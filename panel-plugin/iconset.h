/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef ICONSET_H_INCLUDED
#define ICONSET_H_INCLUDED

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct _MessengerIconSet            MessengerIconSet;
typedef struct _MessengerIconSetClass       MessengerIconSetClass;

#define MESSENGER_TYPE_ICON_SET             ( messenger_icon_set_get_type() )
#define MESSENGER_ICON_SET( o )             ( G_TYPE_CHECK_INSTANCE_CAST( (o), \
                                              MESSENGER_TYPE_ICON_SET, \
                                              MessengerIconSet ) )
#define MESSENGER_ICON_SET_CLASS( k )       ( G_TYPE_CHECK_CLASS_CAST( (k), \
                                              MESSENGER_TYPE_ICON_SET, \
                                              MessengerIconSetClass ) )
#define MESSENGER_IS_ICON_SET( o )          ( G_TYPE_CHECK_INSTANCE_TYPE( (o), \
                                              MESSENGER_TYPE_ICON_SET ) )
#define MESSENGER_IS_ICON_SET_CLASS( k )    ( G_TYPE_CHECK_CLASS_TYPE( (k), \
                                              MESSENGER_TYPE_ICON_SET ) )
#define MESSENGER_ICON_SET_GET_CLASS(o)     ( G_TYPE_INSTANCE_GET_CLASS( (o), \
                                              MESSENGER_TYPE_ICON_SET, \
                                              MessengerIconSetClass ) )

GType               messenger_icon_set_get_type     ( void ) G_GNUC_CONST;
MessengerIconSet    *messenger_icon_set_new         ( void );
void                messenger_icon_set_add_icon     ( MessengerIconSet      *mis,
                                                      const gchar           *identifier,
                                                      const gchar           *source );
void                messenger_icon_set_remove_icon  ( MessengerIconSet      *mis,
                                                      const gchar           *identifier );
gboolean            messenger_icon_set_has_icon     ( MessengerIconSet      *mis,
                                                      const gchar           *identifier );
typedef void (*MessengerIconSetForeachFunc)         ( const gchar           *identifier,
                                                      const gchar           *source,
                                                      gpointer              user_data );
void                messenger_icon_set_foreach      ( MessengerIconSet      *mis,
                                                      MessengerIconSetForeachFunc   func,
                                                      gpointer              user_data );
/*
GdkPixbuf           *messenger_icon_set_render_icon ( const gchar           *identifier,
                                                      GtkIconSize           size,
                                                      GtkWidget             *widget );
*/
gboolean            messenger_icon_set_identifier_is_valid( const gchar *id );

G_END_DECLS

#endif

