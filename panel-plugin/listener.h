/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef LISTENER_H_INCLUDED
#define LISTENER_H_INCLUDED

#include <glib-object.h>

G_BEGIN_DECLS

#define MESSENGER_TYPE_LISTENER             ( messenger_listener_get_type() )
#define MESSENGER_LISTENER( obj )           ( G_TYPE_CHECK_INSTANCE_CAST( (obj), \
                                              MESSENGER_TYPE_LISTENER, MessengerListener ) )
#define MESSENGER_LISTENER_CLASS( klass )   ( G_TYPE_CHECK_CLASS_CAST( (klass), \
                                              MESSENGER_TYPE_LISTENER, MessengerListenerClass ) )
#define MESSENGER_IS_LISTENER( obj )        ( G_TYPE_CHECK_INSTANCE_TYPE( (obj), \
                                              MESSENGER_TYPE_LISTENER ) )
#define MESSENGER_IS_LISTENER_CLASS( klass )( G_TYPE_CHECK_CLASS_TYPE( (klass), \
                                              MESSENGER_TYPE_LISTENER ) )
#define MESSENGER_LISTENER_GET_CLASS( obj ) ( G_TYPE_INSTANCE_GET_CLASS( (obj), \
                                              MESSENGER_TYPE_LISTENER, MessengerListenerClass ) )

typedef struct _MessengerListener           MessengerListener;
typedef struct _MessengerListenerClass      MessengerListenerClass;
typedef struct _MessengerListenerPriv       MessengerListenerPriv;

struct _MessengerListener {
    GObject             parent_instance;

    MessengerListenerPriv   *priv;
};

struct _MessengerListenerClass {
    GObjectClass        parent_class;

    void (*message)( MessengerListener *messenger, guint timeout, const gchar *message );
};

GType               messenger_listener_get_type     ( void ) G_GNUC_CONST;
MessengerListener   *messenger_listener_new         ( void );

#define MESSENGER_LISTENER_DBUS_BUS_NAME        "org.xfce.Panel.Plugin.Messenger"
#define MESSENGER_LISTENER_DBUS_PATH            "/org/xfce/panel/plugin/messenger"
#define MESSENGER_LISTENER_DBUS_INTERFACE       MESSENGER_LISTENER_DBUS_BUS_NAME
#define MESSENGER_LISTENER_DBUS_SIGNAL_MESSAGE  "Message"

G_END_DECLS

#endif /* LISTENER_H_INCLUDED */

