/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include <glib-object.h>
#include <gtk/gtk.h>

#include "iconset.h"

#ifdef DEBUG
#define TRACE_IN()          ( g_message( "%s -->>", __FUNCTION__ ) )
#define TRACE_OUT()         ( g_message( "<<-- %s", __FUNCTION__ ) )
#else
#define TRACE_IN()
#define TRACE_OUT()
#endif

static void     messenger_icon_set_class_init           ( MessengerIconSetClass *klass );
static void     messenger_icon_set_instance_init        ( MessengerIconSet      *icon_set );
static void     messenger_icon_set_finalize             ( GObject               *object );
static void     messenger_icon_set_free_item            ( gpointer              key,
                                                          gpointer              value,
                                                          gpointer              user_data );


struct _MessengerIconSet {
    GObject                 parent_instance;

    GHashTable              *icon_hash;
    GtkIconFactory          *icon_factory;
};

struct _MessengerIconSetClass {
    GObjectClass            parent_class;
};

static GObjectClass             *parent_class = NULL;

GType
messenger_icon_set_get_type( void )
{
    static GType        messenger_icon_set_type = 0;

    if ( !messenger_icon_set_type ) {
        const GTypeInfo     messenger_icon_set_type_info = {
            sizeof( MessengerIconSetClass ),
            NULL,
            NULL,
            (GClassInitFunc) messenger_icon_set_class_init,
            NULL,
            NULL,
            sizeof( MessengerIconSet ),
            0,
            (GInstanceInitFunc) messenger_icon_set_instance_init,
            NULL
        };

        messenger_icon_set_type =
            g_type_register_static( G_TYPE_OBJECT, "MessengerIconSet",
                                    &messenger_icon_set_type_info, 0 );
    }
    return ( messenger_icon_set_type );
}

static void
messenger_icon_set_class_init( MessengerIconSetClass *klass )
{
    GObjectClass        *object_class = G_OBJECT_CLASS( klass );

    TRACE_IN();

    parent_class = g_type_class_peek_parent( klass );

    object_class->finalize      = messenger_icon_set_finalize;
}

static void
messenger_icon_set_instance_init( MessengerIconSet *icon_set )
{
    TRACE_IN();

    icon_set->icon_hash = g_hash_table_new( g_str_hash, g_str_equal );
    icon_set->icon_factory = gtk_icon_factory_new();

    gtk_icon_factory_add_default( icon_set->icon_factory );

    TRACE_OUT();
}

static void
messenger_icon_set_free_item( gpointer key, gpointer value, gpointer user_data )
{
    g_free( key );
    g_free( value );
}

static void
messenger_icon_set_finalize( GObject *object )
{
    MessengerIconSet        *mis = MESSENGER_ICON_SET( object );

    TRACE_IN();

    g_hash_table_foreach( mis->icon_hash, messenger_icon_set_free_item, NULL );
    g_hash_table_destroy( mis->icon_hash );
    
    gtk_icon_factory_remove_default( mis->icon_factory );
    g_object_unref( G_OBJECT( mis->icon_factory ) );

    G_OBJECT_CLASS( parent_class )->finalize( object );

    TRACE_OUT();
}


MessengerIconSet *
messenger_icon_set_new( void )
{
    return ( g_object_new( MESSENGER_TYPE_ICON_SET, NULL ) );
}

void
messenger_icon_set_add_icon( MessengerIconSet *mis,
                             const gchar *identifier,
                             const gchar *source )
{
    gpointer            old_key = NULL, old_value = NULL;
    GtkIconSource       *icon_source;
    GtkIconSet          *icon_set;

    g_return_if_fail( MESSENGER_IS_ICON_SET( mis ) );
    g_return_if_fail( identifier != NULL && *identifier != 0 );
    g_return_if_fail( source != NULL && *source != 0 );

    TRACE_IN();

    if ( g_hash_table_lookup_extended( mis->icon_hash, identifier, &old_key, &old_value ) ) {
        if ( !strcmp( source, old_value ) ) {
            /* Old and new values are identical */
            return;
        }
        else {
            g_free( old_value );
        }
    }

    g_hash_table_insert( mis->icon_hash,
                         old_key ? old_key : g_strdup( identifier ),
                         g_strdup( source ) );

    icon_source = gtk_icon_source_new();

    if ( g_path_is_absolute( source ) ) {
        gtk_icon_source_set_filename( icon_source, source );
    }
    else {
        gtk_icon_source_set_icon_name( icon_source, source );
    }

    icon_set = gtk_icon_set_new();
    gtk_icon_set_add_source( icon_set, icon_source );

    gtk_icon_source_free( icon_source );

    gtk_icon_factory_add( mis->icon_factory, identifier, icon_set );

    gtk_icon_set_unref( icon_set );

    TRACE_OUT();
}

void
messenger_icon_set_remove_icon( MessengerIconSet *mis,
                                const gchar *identifier )
{
    GHashTable          *icon_factory_hash;
    gpointer            key = NULL, value = NULL;

    TRACE_IN();

    g_return_if_fail( MESSENGER_IS_ICON_SET( mis ) );
    g_return_if_fail( identifier != NULL );

    if ( g_hash_table_lookup_extended( mis->icon_hash, identifier, &key, &value ) ) {
        g_hash_table_remove( mis->icon_hash, identifier );

        g_free( key );
        g_free( value );
    }

    /* I'm going to hell for this... */
    icon_factory_hash = mis->icon_factory->icons;

    if ( g_hash_table_lookup_extended( icon_factory_hash, identifier, &key, &value ) ) {
        g_hash_table_remove( icon_factory_hash, identifier );

        g_free( key );
        gtk_icon_set_unref( value );
    }

    TRACE_OUT();
}

gboolean
messenger_icon_set_has_icon( MessengerIconSet *mis,
                             const gchar *identifier )
{
    return ( g_hash_table_lookup( mis->icon_hash, identifier ) != NULL );
}

void
messenger_icon_set_foreach( MessengerIconSet *mis,
                            MessengerIconSetForeachFunc func,
                            gpointer user_data )
{
    g_hash_table_foreach( mis->icon_hash, (GHFunc) func, user_data );
}

/*
GdkPixbuf *messenger_icon_set_render_icon( const gchar      *identifier,
                                           GtkIconSize      size,
                                           GtkWidget        *widget )
{
    GtkIconSet          *icon_set;
    GdkPixbuf           *pb;

    if ( !identifier ) {
        identifier = "";
    }

    if ( size <= GTK_ICON_SIZE_INVALID ) {
        size = -1;
    }

    if ( !widget ) {
        widget = gtk_invisible_new();
        g_object_ref( G_OBJECT( widget ) );
        gtk_object_sink( GTK_OBJECT( widget ) );
    }
    else {
        g_object_ref( G_OBJECT( widget ) );
    }

    icon_set = gtk_style_lookup_icon_set( widget->style, identifier );
    if ( !icon_set ) {
        icon_set = gtk_icon_set_new();
    }
    else {
        gtk_icon_set_ref( icon_set );
    }

    pb = gtk_icon_set_render_icon( icon_set,
                                   widget->style,
                                   gtk_widget_get_direction( widget ),
                                   GTK_WIDGET_STATE( widget ),
                                   size,
                                   widget,
                                   NULL );

    g_assert( pb != NULL );

    gtk_icon_set_unref( icon_set );
    g_object_unref( G_OBJECT( widget ) );

    return ( pb );
}
*/

gboolean
messenger_icon_set_identifier_is_valid( const char *id )
{
    g_return_val_if_fail( id != NULL, FALSE );

    if ( !*id ) {
        return ( FALSE );
    }

    while ( *id ) {
        if ( !g_ascii_isalnum( *id ) && *id != '-' && *id != '_' ) {
            break;
        }
        id++;
    }
    return ( *id == 0 );
}


