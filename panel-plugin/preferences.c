/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "preferences.h"

struct _MessengerPreferences {
    GObject             parent_instance;

    gboolean            show_icon;
    gboolean            show_label;
    guint               label_width;
    guint               message_duration;

    gboolean            show_popup;
    guint               popup_duration;

    gboolean            enable_icons;

    gboolean            save_history;
};

struct _MessengerPreferencesClass {
    GObjectClass        parent_class;
};

static void     messenger_preferences_class_init        ( MessengerPreferencesClass *klass );
static void     messenger_preferences_init              ( MessengerPreferences  *prefs );
static void     messenger_preferences_set_property      ( GObject               *object,
                                                          guint                 property_id,
                                                          const GValue          *value,
                                                          GParamSpec            *pspec );
static void     messenger_preferences_get_property      ( GObject               *object,
                                                          guint                 property_id,
                                                          GValue                *value,
                                                          GParamSpec            *pspec );
static void     messenger_preferences_finalize          ( GObject               *object );

enum {
    PROP_0,
    PROP_SHOW_ICON,
    PROP_SHOW_LABEL,
    PROP_LABEL_WIDTH,
    PROP_MESSAGE_DURATION,
    PROP_SHOW_POPUP,
    PROP_POPUP_DURATION,
    PROP_SAVE_HISTORY
};

static GObjectClass             *messenger_preferences_parent_class = NULL;
static MessengerPreferences     *messenger_preferences_singleton = NULL;

GType
messenger_preferences_get_type( void )
{
    static GType        messenger_preferences_type = 0;

    if ( !messenger_preferences_type ) {
        GTypeInfo       messenger_preferences_typeinfo = {
            sizeof( MessengerPreferencesClass ),
            NULL,
            NULL,
            (GClassInitFunc) messenger_preferences_class_init,
            NULL,
            NULL,
            sizeof( MessengerPreferences ),
            0,
            (GInstanceInitFunc) messenger_preferences_init,
            NULL
        };

        messenger_preferences_type =
            g_type_register_static( G_TYPE_OBJECT, "MessengerPreferences",
                                    &messenger_preferences_typeinfo, 0 );
    }
    return ( messenger_preferences_type );
}

static void
messenger_preferences_class_init( MessengerPreferencesClass *klass )
{
    GObjectClass            *gobject_class;
    const GParamFlags       property_flags = G_PARAM_CONSTRUCT | G_PARAM_READWRITE;
    
    messenger_preferences_parent_class = g_type_class_peek_parent( klass );

    gobject_class       = G_OBJECT_CLASS( klass );

    gobject_class->set_property     = messenger_preferences_set_property;
    gobject_class->get_property     = messenger_preferences_get_property;
    gobject_class->finalize         = messenger_preferences_finalize;

    g_object_class_install_property( gobject_class,
                                     PROP_SHOW_ICON,
                                     g_param_spec_boolean( "show-icon",
                                                           "Show Icon",
                                                           "Show Icon",
                                                           PREF_SHOW_ICON_DEFAULT,
                                                           property_flags ) );
    g_object_class_install_property( gobject_class,
                                     PROP_SHOW_LABEL,
                                     g_param_spec_boolean( "show-label",
                                                           "Show Label",
                                                           "Show Label",
                                                           PREF_SHOW_LABEL_DEFAULT,
                                                           property_flags ) );
    g_object_class_install_property( gobject_class,
                                     PROP_LABEL_WIDTH,
                                     g_param_spec_uint( "label-width",
                                                        "Label Width",
                                                        "Label Width",
                                                        PREF_MIN_LABEL_WIDTH,
                                                        PREF_MAX_LABEL_WIDTH,
                                                        PREF_LABEL_WIDTH_DEFAULT,
                                                        property_flags ) );
    g_object_class_install_property( gobject_class,
                                     PROP_MESSAGE_DURATION,
                                     g_param_spec_uint( "message-duration",
                                                        "Message Duration",
                                                        "Message Duration",
                                                        1,
                                                        3600,
                                                        PREF_MESSAGE_DURATION_DEFAULT,
                                                        property_flags ) );
    g_object_class_install_property( gobject_class,
                                     PROP_SHOW_POPUP,
                                     g_param_spec_boolean( "show-popup",
                                                           "Show Popup",
                                                           "Show Popup",
                                                           PREF_SHOW_POPUP_DEFAULT,
                                                           property_flags ) );
    g_object_class_install_property( gobject_class,
                                     PROP_POPUP_DURATION,
                                     g_param_spec_uint( "popup-duration",
                                                        "Popup Duration",
                                                        "Popup Duration",
                                                        PREF_MIN_POPUP_DURATION,
                                                        PREF_MAX_POPUP_DURATION,
                                                        PREF_POPUP_DURATION_DEFAULT,
                                                        property_flags ) );
    g_object_class_install_property( gobject_class,
                                     PROP_SAVE_HISTORY,
                                     g_param_spec_boolean( "save-history",
                                                           "Save History",
                                                           "Save History",
                                                           PREF_SAVE_HISTORY_DEFAULT,
                                                           property_flags ) );
}

static void
messenger_preferences_init( MessengerPreferences  *prefs )
{
    /* ? */
}

static void
messenger_preferences_set_property( GObject *object,
                                    guint property_id,
                                    const GValue *value,
                                    GParamSpec *pspec )
{
    MessengerPreferences        *prefs = MESSENGER_PREFERENCES( object );
 
    switch ( property_id ) {
        case PROP_SHOW_ICON:
            prefs->show_icon = g_value_get_boolean( value );
            break;
            
        case PROP_SHOW_LABEL:
            prefs->show_label = g_value_get_boolean( value );
            break;

        case PROP_LABEL_WIDTH:
            prefs->label_width = g_value_get_uint( value );
            break;

        case PROP_MESSAGE_DURATION:
            prefs->message_duration = g_value_get_uint( value );
            break;

        case PROP_SHOW_POPUP:
            prefs->show_popup = g_value_get_boolean( value );
            break;

        case PROP_POPUP_DURATION:
            prefs->popup_duration = g_value_get_uint( value );
            break;

        case PROP_SAVE_HISTORY:
            prefs->save_history = g_value_get_boolean( value );
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID( object, property_id, pspec );
            break;
    }
}

static void
messenger_preferences_get_property( GObject *object,
                                    guint property_id,
                                    GValue *value,
                                    GParamSpec *pspec )
{
    MessengerPreferences        *prefs = MESSENGER_PREFERENCES( object );

    switch ( property_id ) {
        case PROP_SHOW_ICON:
            g_value_set_boolean( value, prefs->show_icon );
            break;

        case PROP_SHOW_LABEL:
            g_value_set_boolean( value, prefs->show_label );
            break;

        case PROP_LABEL_WIDTH:
            g_value_set_uint( value, prefs->label_width );
            break;

        case PROP_MESSAGE_DURATION:
            g_value_set_uint( value, prefs->message_duration );
            break;

        case PROP_SHOW_POPUP:
            g_value_set_boolean( value, prefs->show_popup );
            break;

        case PROP_POPUP_DURATION:
            g_value_set_uint( value, prefs->popup_duration );
            break;

        case PROP_SAVE_HISTORY:
            g_value_set_boolean( value, prefs->save_history );
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID( object, property_id, pspec );
            break;
    }
}

static void
messenger_preferences_finalize( GObject *object )
{
    messenger_preferences_singleton = NULL;

    G_OBJECT_CLASS( messenger_preferences_parent_class )->finalize( object );
}

/* ============================== PUBLIC ================================ */
MessengerPreferences *
messenger_preferences_get( void )
{
   
    if ( G_UNLIKELY( messenger_preferences_singleton == NULL ) ) {
        messenger_preferences_singleton = g_object_new( MESSENGER_TYPE_PREFERENCES, NULL );
    }
    else {
        g_object_ref( messenger_preferences_singleton );
    }

    return ( messenger_preferences_singleton );
}


