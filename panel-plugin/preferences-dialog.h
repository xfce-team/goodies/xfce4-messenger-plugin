/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PREFERENCES_DIALOG_H_INCLUDED
#define PREFERENCES_DIALOG_H_INCLUDED

#include <gtk/gtk.h>

#include "preferences.h"
#include "iconset.h"

G_BEGIN_DECLS

typedef struct _MessengerPreferencesDialog              MessengerPreferencesDialog;
typedef struct _MessengerPreferencesDialogClass         MessengerPreferencesDialogClass;

#define MESSENGER_TYPE_PREFERENCES_DIALOG               ( messenger_preferences_dialog_get_type() )
#define MESSENGER_PREFERENCES_DIALOG( obj )             ( G_TYPE_CHECK_INSTANCE_CAST( (obj), \
                                                          MESSENGER_TYPE_PREFERENCES_DIALOG, \
                                                          MessengerPreferencesDialog ) )
#define MESSENGER_IS_PREFERENCES_DIALOG( obj )          ( G_TYPE_CHECK_INSTANCE_TYPE( (obj), \
                                                          MESSENGER_TYPE_PREFERENCES_DIALOG ) )
#define MESSENGER_PREFERENCES_DIALOG_CLASS( klass )     ( G_TYPE_CHECK_CLASS_CAST( (klass), \
                                                          MESSENGER_TYPE_PREFERENCES_DIALOG, \
                                                          MessengerPreferencesDialogClass ) )
#define MESSENGER_IS_PREFERENCES_DIALOG_CLASS( klass )  ( G_TYPE_CHECK_CLASS_TYPE( (klass), \
                                                          MESSENGER_TYPE_PREFERENCES_DIALOG ) )
#define MESSENGER_PREFERENCES_DIALOG_GET_CLASS( obj )   ( G_TYPE_INSTANCE_GET_CLASS( (obj), \
                                                          MESSENGER_TYPE_PREFERENCES_DIALOG, \
                                                          MessengerPreferencesDialogClass ) )

GType       messenger_preferences_dialog_get_type       ( void ) G_GNUC_CONST;
GtkWidget   *messenger_preferences_dialog_new           ( MessengerPreferences *prefs );
void        messenger_preferences_dialog_set_icon_set   ( MessengerPreferencesDialog *dialog,
                                                          MessengerIconSet *icon_set );
MessengerIconSet *messenger_preferences_dialog_get_icon_set( MessengerPreferencesDialog *dialog );


G_END_DECLS

#endif

