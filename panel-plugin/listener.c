/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <glib-object.h>

#define DBUS_API_SUBJECT_TO_CHANGE 1
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "listener.h"

#define LISTENER_DBUS_TRY_CONNECT_TIMEOUT       ( 20 * 1000 )

#define MESSENGER_LISTENER_GET_PRIVATE( obj )   ( G_TYPE_INSTANCE_GET_PRIVATE( (obj), \
                                                  MESSENGER_TYPE_LISTENER, MessengerListenerPriv ) )

#ifdef DEBUG
#define TRACE()             ( g_message( "%s", __FUNCTION__ ) )
#else
#define TRACE()
#endif

struct _MessengerListenerPriv {
    guint               timeout_id;

    DBusConnection      *connection;

    guint               finalizing:1;
};

enum {
    MESSAGE,
    NUM_SIGNALS
};

static void                 messenger_listener_class_init       ( MessengerListenerClass *klass );
static void                 messenger_listener_instance_init    ( MessengerListener *messenger );
static void                 messenger_listener_finalize         ( GObject *object );

static void                 _dbus_disconnect                    ( MessengerListener *messenger );
static gboolean             _dbus_connect                       ( MessengerListener *messenger );
static gboolean             _dbus_try_connect_timeout_cb        ( gpointer user_data );
static DBusHandlerResult    _dbus_message_cb                    ( DBusConnection *connection,
                                                                  DBusMessage *message,
                                                                  void *user_data );

static gpointer     messenger_listener_parent_class = NULL;
static guint        messenger_listener_signals[NUM_SIGNALS];

GType
messenger_listener_get_type( void )
{
    static GType        messenger_listener_type = 0;

    if ( !messenger_listener_type ) {
        /* This is deep copied so static is unnecessary */
        GTypeInfo       messenger_listener_type_info = {
            sizeof( MessengerListenerClass ),
            NULL,       /* base_init */
            NULL,       /* base_finalize */
            (GClassInitFunc) messenger_listener_class_init,
            NULL,       /* class_finalize */
            NULL,       /* class_data */
            sizeof( MessengerListener ),
            0,
            (GInstanceInitFunc) messenger_listener_instance_init,
            NULL
        };

        messenger_listener_type = g_type_register_static( G_TYPE_OBJECT,
                                                      "MessengerListener",
                                                      &messenger_listener_type_info,
                                                      0 );
    }
    return ( messenger_listener_type );
}

static void
messenger_listener_class_init( MessengerListenerClass *klass )
{
    GObjectClass        *object_class;
    
    messenger_listener_parent_class = g_type_class_peek_parent( klass );

    object_class = G_OBJECT_CLASS( klass );
    object_class->finalize      = messenger_listener_finalize;

    klass->message              = NULL;

    g_type_class_add_private( klass, sizeof( MessengerListenerPriv ) );

    messenger_listener_signals[MESSAGE] =
        g_signal_new( "message",
                      G_TYPE_FROM_CLASS( klass ),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET( MessengerListenerClass, message ),
                      NULL, NULL,
                      g_cclosure_marshal_VOID__UINT_POINTER,
                      G_TYPE_NONE,
                      2,
                      G_TYPE_INT,
                      G_TYPE_STRING );

}

static void
messenger_listener_instance_init( MessengerListener *messenger )
{
    MessengerListenerPriv       *priv = MESSENGER_LISTENER_GET_PRIVATE( messenger );

    priv->finalizing = 0;

    if ( !_dbus_connect( messenger ) ) {
        g_timeout_add( LISTENER_DBUS_TRY_CONNECT_TIMEOUT,
                       _dbus_try_connect_timeout_cb,
                       messenger );
    }
}

static void
messenger_listener_finalize( GObject *object )
{
    MessengerListener           *messenger = MESSENGER_LISTENER( object );
    MessengerListenerPriv       *priv = MESSENGER_LISTENER_GET_PRIVATE( messenger );

    priv->finalizing    = 1;

    _dbus_disconnect( messenger );

    G_OBJECT_CLASS( messenger_listener_parent_class )->finalize( object );
}

static void
_dbus_disconnect( MessengerListener *messenger )
{
    MessengerListenerPriv       *priv = MESSENGER_LISTENER_GET_PRIVATE( messenger );

    TRACE();

    if ( priv->timeout_id ) {
        g_source_remove( priv->timeout_id );
        priv->timeout_id = 0;
    }
    if ( priv->connection ) {
        DBusConnection      *connection = priv->connection;

        priv->connection = NULL;

        dbus_connection_remove_filter( connection, _dbus_message_cb, messenger );
        
        if ( dbus_connection_get_is_connected( connection ) ) {
            dbus_connection_close( connection );
        }

        dbus_connection_unref( connection );
    }

    if ( !priv->finalizing ) {
        /* We've disconnected for some other reason.
         * Try resuming */
        g_timeout_add( LISTENER_DBUS_TRY_CONNECT_TIMEOUT,
                       _dbus_try_connect_timeout_cb,
                       messenger );
    }
}

static gboolean
_dbus_connect( MessengerListener *messenger )
{
    MessengerListenerPriv    *priv = MESSENGER_LISTENER_GET_PRIVATE( messenger );
    DBusConnection       *connection;
    DBusError            error;

    TRACE();

    g_return_val_if_fail( priv->connection == NULL, TRUE );

    dbus_error_init( &error );

    connection = dbus_bus_get( DBUS_BUS_SESSION, &error );
    if ( !connection ) {
        g_critical( "xfce-messenger-plugin: Failed to connect DBus: %s", error.message );

        dbus_error_free( &error );

        return ( FALSE );
    }

    dbus_connection_setup_with_g_main( connection, NULL );
    dbus_connection_set_exit_on_disconnect( connection, FALSE );

    if ( dbus_bus_request_name( connection, MESSENGER_LISTENER_DBUS_BUS_NAME, 0, &error ) < 0 ) {
        g_critical( "xfce-messenger-plugin: Failed to register DBus path: %s", error.message );
        
        dbus_connection_close( connection );
        dbus_connection_unref( connection );

        return ( FALSE );
    }

    if ( !dbus_connection_add_filter( connection, _dbus_message_cb, messenger, NULL ) ) {
        g_warning( "xfce-messenger-plugin: Failed to add Dbus connection filter: %s",
                   error.message );
    }

    priv->connection = connection;

    return ( TRUE );
}

static gboolean
_dbus_try_connect_timeout_cb( gpointer user_data )
{
    TRACE();
    
    return ( !_dbus_connect( user_data ) );
}

static DBusHandlerResult
_dbus_message_cb( DBusConnection *connection,
                  DBusMessage *message,
                  void *user_data )
{
    MessengerListener           *messenger = user_data;
    DBusHandlerResult       retval = DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

#ifdef DEBUG
    g_message( "%s", dbus_message_get_path( message ) );
    g_message( "%s", dbus_message_get_interface( message ) );
    g_message( "%s", dbus_message_get_member( message ) );
#endif

    if ( dbus_message_is_signal( message,
                                 DBUS_INTERFACE_LOCAL,
                                 "Disconnected" ) ) {
        _dbus_disconnect( messenger );
    }
    else if ( dbus_message_is_signal( message,
                                      DBUS_INTERFACE_DBUS,
                                      "NameAcquired" ) ) {
        retval = DBUS_HANDLER_RESULT_HANDLED;
    }
    else if ( dbus_message_is_signal( message,
                                      MESSENGER_LISTENER_DBUS_BUS_NAME,
                                      MESSENGER_LISTENER_DBUS_SIGNAL_MESSAGE ) ) {
        DBusMessageIter     iter;

        if ( dbus_message_iter_init( message, &iter ) ) {
            gint            arg_type;
            dbus_uint32_t    timeout = 0;
            const char      *msgstr = NULL;

            while ( ( arg_type = dbus_message_iter_get_arg_type( &iter ) ) != DBUS_TYPE_INVALID ) {
                if ( arg_type == DBUS_TYPE_UINT32 ) {
                    dbus_message_iter_get_basic( &iter, &timeout );
                }
                else if ( arg_type == DBUS_TYPE_STRING ) {
                    gchar       *s;
                    
                    dbus_message_iter_get_basic( &iter, &msgstr );

                    s = g_strdup( msgstr );
                    
                    g_signal_emit( messenger,
                                   messenger_listener_signals[MESSAGE],
                                   0,
                                   timeout,
                                   s );

                    g_free( s );
                    
                    if ( timeout ) {
                        timeout = 0;
                    }
                }

                dbus_message_iter_next( &iter );
            }

        }
        retval = DBUS_HANDLER_RESULT_HANDLED;
    }

    return ( retval );
}

/* ===================== PUBLIC API ===================== */

MessengerListener *
messenger_listener_new( void )
{
    return ( g_object_new( MESSENGER_TYPE_LISTENER, NULL ) );
}

