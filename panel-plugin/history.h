/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef HISTORY_H_INCLUDED
#define HISTORY_H_INCLUDED

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct _MessengerHistory        MessengerHistory;


MessengerHistory    *messenger_history_new                  ( void );
void                messenger_history_message_prepend       ( MessengerHistory  *history,
                                                              const gchar       *message );
void                messenger_history_message_append        ( MessengerHistory  *history,
                                                              const gchar       *message );
void                messenger_history_message_prepend_raw   ( MessengerHistory  *history,
                                                              const gchar       *message,
                                                              const gchar       *timestr );
void                messenger_history_message_append_raw    ( MessengerHistory  *history,
                                                              const gchar       *message,
                                                              const gchar       *timestr );
void                messenger_history_set_length            ( MessengerHistory  *history,
                                                              guint             length );
guint               messenger_history_get_length            ( MessengerHistory  *history );
GtkWidget           *messenger_history_create_dialog        ( MessengerHistory  *history,
                                                              GtkWindow         *parent_window );
void                messenger_history_free                  ( MessengerHistory  *history );
typedef void        (*MessengerHistoryForeachFunc)          ( guint             index,
                                                              const gchar       *message,
                                                              const gchar       *timestr,
                                                              gpointer          user_data );
void                messenger_history_foreach               ( MessengerHistory  *history,
                                                              MessengerHistoryForeachFunc func,
                                                              gpointer          user_data );

G_END_DECLS

#endif
