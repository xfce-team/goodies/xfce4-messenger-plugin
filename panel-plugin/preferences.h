/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PREFERENCES_H_INCLUDED
#define PREFERENCES_H_INCLUDED

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _MessengerPreferences            MessengerPreferences;
typedef struct _MessengerPreferencesClass       MessengerPreferencesClass;

#define MESSENGER_TYPE_PREFERENCES                          ( messenger_preferences_get_type() )
#define MESSENGER_PREFERENCES( obj )                        ( G_TYPE_CHECK_INSTANCE_CAST( (obj), \
                                                              MESSENGER_TYPE_PREFERENCES, \
                                                              MessengerPreferences ) )
#define MESSENGER_IS_PREFERENCES( obj )                     ( G_TYPE_CHECK_INSTANCE_TYPE( (obj), \
                                                              MESSENGER_TYPE_PREFERENCES ) )
#define MESSENGER_PREFERENCES_CLASS( klass )                ( G_TYPE_CHECK_CLASS_CAST( (klass), \
                                                              MESSENGER_TYPE_PREFERENCES, \
                                                              MessengerPreferencesClass ) )
#define MESSENGER_IS_PREFERENCES_CLASS( klass )             ( G_TYPE_CHECK_CLASS_TYPE( (klass), \
                                                              MESSENGER_TYPE_PREFERENCES ) )
#define MESSENGER_PREFERENCES_GET_CLASS( obj )              ( G_TYPE_INSTANCE_GET_CLASS( (obj), \
                                                              MESSENGER_TYPE_PREFERENCES, \
                                                              MessengerPreferencesClass ) )

GType                   messenger_preferences_get_type      ( void ) G_GNUC_CONST;
MessengerPreferences    *messenger_preferences_get          ( void );

/* Default values */
#define PREF_SHOW_ICON_DEFAULT          TRUE
#define PREF_SHOW_LABEL_DEFAULT         TRUE
#define PREF_LABEL_WIDTH_DEFAULT        120
#define PREF_MIN_LABEL_WIDTH            20
#define PREF_MAX_LABEL_WIDTH            1024
#define PREF_MESSAGE_DURATION_DEFAULT   5
#define PREF_MIN_POPUP_DURATION         1
#define PREF_MAX_POPUP_DURATION         60
#define PREF_SHOW_POPUP_DEFAULT         TRUE
#define PREF_POPUP_DURATION_DEFAULT     5
#define PREF_SAVE_HISTORY_DEFAULT       TRUE

G_END_DECLS

#endif

