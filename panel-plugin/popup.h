/* 
 *  Xfce4 Messenger Plugin - DBus message notification plugin for Xfce4 Panel
 *  Copyright (C) 2005-2006  Pasi Orovuo <pasi.ov@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef POPUP_H_INCLUDED
#define POPUP_H_INCLUDED

#include <glib-object.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

G_BEGIN_DECLS

typedef struct _MessengerPopup                  MessengerPopup;
typedef struct _MessengerPopupClass             MessengerPopupClass;


#define MESSENGER_TYPE_POPUP                    ( messenger_popup_get_type() )
#define MESSENGER_POPUP( o )                    ( G_TYPE_CHECK_INSTANCE_CAST( (o), \
                                                  MESSENGER_TYPE_POPUP, \
                                                  MessengerPopup ) )
#define MESSENGER_IS_POPUP( o )                 ( G_TYPE_CHECK_INSTANCE_TYPE( (o), \
                                                  MESSENGER_TYPE_POPUP ) )
#define MESSENGER_POPUP_CLASS( k )              ( G_TYPE_CHECK_CLASS_CAST( (k), \
                                                  MESSENGER_TYPE_POPUP, \
                                                  MessengerPopupClass ) )
#define MESSENGER_IS_POPUP_CLASS( k )           ( G_TYPE_CHECK_CLASS_TYPE( (k), \
                                                  MESSENGER_TYPE_POPUP ) )
#define MESSENGER_POPUP_GET_CLASS( o )          ( G_TYPE_INSTANCE_GET_CLASS( (o), \
                                                  MESSENGER_TYPE_POPUP, \
                                                  MessengerPopupClass ) )

typedef enum {
    MESSENGER_POPUP_POSITION_NW = 0,
    MESSENGER_POPUP_POSITION_NE,
    MESSENGER_POPUP_POSITION_SW,
    MESSENGER_POPUP_POSITION_SE
} MessengerPopupPosition;

GType                   messenger_popup_get_type        ( void ) G_GNUC_CONST;
GtkWidget               *messenger_popup_new            ( void );
void                    messenger_popup_set_timeout     ( MessengerPopup    *popup,
                                                          guint timeout );
guint                   messenger_popup_get_timeout     ( MessengerPopup    *popup );
void                    messenger_popup_set_position    ( MessengerPopup    *popup,
                                                          MessengerPopupPosition position );
MessengerPopupPosition  messenger_popup_get_position    ( MessengerPopup    *popup );
void                    messenger_popup_text            ( MessengerPopup    *popup,
                                                          const gchar       *text,
                                                          GdkPixbuf         *icon );


G_END_DECLS

#endif

